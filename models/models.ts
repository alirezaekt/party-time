export const enum Status {
  NoResponse = 'no_response',
  Going = 'going',
  NotGoing = 'not_going',
}
