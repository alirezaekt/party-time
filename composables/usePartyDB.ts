// @ts-nocheck
// todo: added above line to ignore the from type issue until it is resolved

import { nanoid } from 'nanoid';
import { Status } from '~/models/models';

export const usePartyDB = () => {
  const supabase = useSupabaseClient();

  const generateInvitelink = async ({
    displayName,
    fullName,
    partyId,
  }: {
    displayName: string;
    fullName?: string;
    partyId: string;
  }) => {
    if (!displayName) {
      throw new Error('Display Name is required.');
    }
    const inviteId = `${displayName.trim()}-${nanoid(6)}`;
    const { data, error } = await supabase
      .from('invitations')
      .insert([
        {
          id: inviteId,
          party_id: partyId,
          display_name: displayName,
          ...(fullName && { full_name: fullName }),
        },
      ])
      .select();
    if (error) {
      console.error('Error generating invite link:', error);
      return null;
    }
    return inviteId;
  };

  const fetchInvitation = async (inviteId: string | string[]) => {
    const { data, error } = await supabase
      .from('invitations')
      .select('display_name, full_name, status')
      .eq('id', inviteId);
    if (error) {
      console.error(error);
    }
    if (!data || !data[0]) {
      throw new Error('This link does not exist or is expired.');
    }
    return data[0];
  }

  const updateStatus = async (inviteId: string | string[], status: Status) => {
    const { data, error } = await supabase
      .from('invitations')
      .update({ status })
      .eq('id', inviteId)
      .select();
    if (error) {
      console.error(error);
    }
    return data[0] as { status: Status };
  }

  const fetchPartyInvitations = async (partyId: string) => {
    const { data, error } = await supabase
      .from('invitations')
      .select('id, display_name, full_name, status')
      .eq('party_id', partyId);
    if (error) {
      console.error(error);
    }
    return data;
  }

  return {
    generateInvitelink,
    fetchInvitation,
    updateStatus,
    fetchPartyInvitations,
  };
}