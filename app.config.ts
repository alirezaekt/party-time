export default defineAppConfig({
    ui: {
        container: {
            constrained: 'max-w-4xl',
        },
        primary: 'blue',
        gray: 'neutral',
    }
})