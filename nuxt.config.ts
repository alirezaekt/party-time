// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  app: {
    head: {
      link: [
        { rel: "icon", href: "data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%2210 0 100 100%22><text y=%22.90em%22 font-size=%2290%22>🎊</text></svg>" }
      ]
    }
  },
  runtimeConfig: {
    public: {
      mainUrl: process.env.NUXT_PUBLIC_MAIN_URL,
    },
  },
  modules: [
    "@nuxtjs/supabase",
    "@nuxt/ui",
    "@nuxt/image",
  ],
  supabase: {
    redirect: false,
  },
  ui: {
    icons: ['heroicons', 'hugeicons'],
  }
})